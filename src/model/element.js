export class Element {
  constructor (boxRow, boxCol, row, col, number = 0) {
    this.BoxRow = boxRow
    this.BoxCol = boxCol
    this.Row = row
    this.Col = col
    if (Number.isInteger(number) && number >= 0 && number <= 9) {
      this.Number = number
    }
  }

  setNumber (number) {
    this.Number = number
  }
}
