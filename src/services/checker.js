export function RowOk (boxRow, row, grid) {
  const testRow = grid.filter(e => e.BoxRow === boxRow && e.Row === row)
  return AreUniqueNumbers(testRow)
}

export function ColOk (boxCol, col, grid) {
  const testCol = grid.filter(e => e.BoxCol === boxCol && e.Col === col)
  return AreUniqueNumbers(testCol)
}

export function BoxOk (boxRow, boxCol, grid) {
  const testBox = grid.filter(e => e.BoxRow === boxRow && e.BoxCol === boxCol)
  return AreUniqueNumbers(testBox)
}

export function AllRowsOk (grid) {
  for (let i = 1; i <= 3; i++) {
    for (let j = 1; j <= 3; j++) {
      if (!RowOk(i, j, grid)) {
        console.log(`Row ${(i - 1) * 3 + j} NOT OK`)
        return false
      }
    }
  }
  console.log('All Rows OK')
  return true
}

export function AllColsOk (grid) {
  for (let i = 1; i <= 3; i++) {
    for (let j = 1; j <= 3; j++) {
      if (!ColOk(i, j, grid)) {
        console.log(`Column ${(i - 1) * 3 + j} NOT OK`)
        return false
      }
    }
  }
  console.log('All Columns OK')
  return true
}

export function AllBoxesOk (grid) {
  for (let i = 1; i <= 3; i++) {
    for (let j = 1; j <= 3; j++) {
      if (!BoxOk(i, j, grid)) {
        console.log(`Box ${(i - 1) * 3 + j} NOT OK`)
        return false
      }
    }
  }
  console.log('All Boxes OK')
  return true
}

export function AllOk (grid) {
  return AllBoxesOk(grid) && AllRowsOk(grid) && AllColsOk(grid)
}

export function AreUniqueNumbers (elements) {
  if (elements.length !== 9) {
    return false
  }
  for (let i = 1; i <= 9; i++) {
    let count = 0
    let x
    for (x of elements) {
      // console.log('checking ', x.Number)
      if (x.Number === i) {
        count++
      }
      if (count > 1) {
        return false
      }
    }
  }
  return true
}

export function CheckTarget (e, grid) {
  const rowOk = RowOk(e.BoxRow, e.Row, grid)
  const colOk = ColOk(e.BoxCol, e.Col, grid)
  const boxOk = BoxOk(e.BoxRow, e.BoxCol, grid)
  return rowOk && colOk && boxOk
}
