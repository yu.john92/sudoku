import { Element } from 'src/model/element'
import { AllOk, CheckTarget } from 'src/services/checker'

function initialState () {
  return {
    grid: [],
    editIndex: -1,
    solvable: true
  }
}

const state = () => initialState()

const getters = {
  grid: state => state.grid,
  editIndex: state => state.editIndex,
  solvable: state => state.solvable
}

const mutations = {
  updateGrid: (state, number) => {
    state.grid[state.editIndex].Number = number
    if (state.grid.filter(e => e.Number === 0).length === 0) {
      state.solvable = false
    } else {
      state.solvable = AllOk(state.grid)
    }
  },
  addToGrid: (state, e) => {
    state.grid.push(e)
  },
  changeEditIndex: (state, index) => {
    state.editIndex = index
  },
  solveGrid: (state) => {
    state.solvable = false
    const grid = state.grid
    const emptyBoxes = grid.filter(e => e.Number === 0)
    console.log('zeros', emptyBoxes.length)
    if (emptyBoxes == null || emptyBoxes.length === 0) { return null }
    let index = 0
    do {
      if (mutations.Increment(emptyBoxes[index])) {
        if (CheckTarget(emptyBoxes[index], grid)) {
          index++
        }
      } else {
        emptyBoxes[index].Number = 0
        if (index > 0) { index-- }
      }
    } while (index !== emptyBoxes.length)
    console.log('done', state.solvable)
  },
  Increment: (e) => {
    if (e.Number + 1 > 9) {
      return false
    } else {
      e.Number += 1
      return true
    }
  },
  resetGrid: (state) => {
    state.grid.forEach(e => { e.Number = 0 })
    console.log(state.grid)
    state.solvable = AllOk(state.grid)
  }
}

const actions = {
  initializeGrid: ({ commit, state }) => {
    let x = 0
    for (let boxRow = 1; boxRow <= 3; boxRow++) {
      for (let boxCol = 1; boxCol <= 3; boxCol++) {
        for (let row = 1; row <= 3; row++) {
          for (let col = 1; col <= 3; col++) {
            const e = new Element(boxRow, boxCol, row, col)
            e.Index = x
            commit('addToGrid', e)
            x++
          }
        }
      }
    }
    AllOk(state.grid)
  },
  changeEditIndex: ({ commit }, index) => { commit('changeEditIndex', index) },
  updateGrid: ({ commit, state }, number) => {
    console.log('upd grid editIndex', state.editIndex)
    if (state.editIndex >= 0 && number >= 0 && number <= 9) {
      commit('updateGrid', number)
    } else {
      console.error('Invalid input value')
    }
  },
  solveGrid: ({ commit }) => { commit('solveGrid') },
  resetGrid: ({ commit }) => { commit('resetGrid') },
  checkBox: ({ state }, index) => {
    return CheckTarget(state.grid[index].Number, state.grid)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
